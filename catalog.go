package model

import "gopkg.in/mgo.v2/bson"

//Category represents mvpp
type Category struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Slug        string        `json:"slug"bson:"slug"`
	Name        string        `json:"name"bson:"name"`
	Icon        string        `json:"icon"bson:"icon"`
	Description string        `json:"description"bson:"description"`
}

//Categories is an array of Category
type Categories []Category

//Provider represents mvpp
type Provider struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Slug        string        `json:"slug"bson:"slug"`
	Name        string        `json:"name"bson:"name"`
	Description string        `json:"description"bson:"description"`
	Timestamp   int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Providers is an array of Provider
type Providers []Provider

//Product represents mvpp
type Product struct {
	ID               bson.ObjectId   `json:"_id,omitempty"bson:"_id,omitempty"`
	Category         Category        `json:"category,omitempty"bson:"category,omitempty"`
	Type             string          `json:"type,omitempty"bson:"type"` // (product/service etc...)
	Slug             string          `json:"slug"bson:"slug"`
	SKU              string          `json:"sku"bson:"sku"`
	Name             string          `json:"name"bson:"name"`
	Description      string          `json:"description"bson:"description"`
	ShortDescription string          `json:"short_description"bson:"short_description"`
	Provider         Provider        `json:"provider"bson:"provider"`
	OfferPrice       string          `json:"offer_price"bson:"offer_price"`
	AllowDiscount    bool            `json:"allow_discount"bson:"allow_discount"`
	Duration         string          `json:"duration"bson:"duration"`
	Availability     string          `json:"availability"bson:"availability"` //(for service 0 everywhere, else distance radius in km ex. 5)
	Medium           string          `json:"medium"bson:"medium"`             //(In-Person/Meeting Media like (webex, lynx etc ...)/In-Class etc ...)
	Status           bool            `json:"status"bson:"status"`
	Pricing          Pricing         `json:"pricing"bson:"pricing"`
	Information      []Information   `json:"information"bson:"information"`
	Specification    []Specification `json:"specification"bson:"specification"`
	Timestamp        int             `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Products is an array of Product
type Products []Product

// Pricing represents mvpp
type Pricing struct {
	ID         bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Product    bson.ObjectId `json:"product,omitempty"bson:"product,omitempty"`
	Type       string        `json:"type"bson:"type"` //(hour/day/full/KG/meter/liter etc...)
	Price      string        `json:"price"bson:"price"`
	OfferPrice string        `json:"offer_price"bson:"offer_price"`
	Currency   string        `json:"currency"bson:"currency"`
	Available  bool          `json:"available"bson:"available"`
	Timestamp  int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Pricing is an array of Pricing
type Pricings []Pricing

//Specification represents mvpp
type Specification struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Product     bson.ObjectId `json:"product,omitempty"bson:"product,omitempty"`
	Name        string        `json:"name"bson:"name"`
	Description string        `json:"description"bson:"description"`
	Timestamp   int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Specifications is an array of Specification
type Specifications []Specification

//Information represents mvpp
type Information struct {
	ID        bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Product   bson.ObjectId `json:"product,omitempty"bson:"product,omitempty"`
	Image     string        `json:"images"bson:"image"`
	Type      string        `json:"type"bson:"type"`
	Timestamp int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Informations is an array of Information
type Informations []Information
