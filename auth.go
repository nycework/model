package model

import (
	"gopkg.in/mgo.v2/bson"
)

type Account struct {
	ID       bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Type     string `json:"type"bson:"type"`
	Pid      string `json:"pid,omitempty"bson:"pid,omitempty"`
	Email    string `json:"email"bson:"email"`
	Phone    string `json:"phone"bson:"phone"`
	Username string `json:"username"bson:"username"`
	Password string `json:"password"bson:"password"`
	Timestamp int    `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

type Profile struct {
	ID       bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Aid      bson.ObjectId `json:"aid,omitempty"bson:"aid,omitempty"`
	Firstname string `json:"firstname"bson:"firstname"`
	Lastname  string `json:"lastname"bson:"lastname"`
	Avatar  string `json:"avatar"bson:"avatar"`
	DOB  int `json:"dob"bson:"dob"`
	Gender string `json:"gender"bson:"gender"`
	Bio string `json:"bio"bson:"bio"`
	Banner string `json:"banner"bson:"banner"`
	Location string `json:"location"bson:"location"`
}
