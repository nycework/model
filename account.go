package model

import (
	"gopkg.in/mgo.v2/bson"
)

type OrgAccount struct {
	ID           bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Organization string        `json:"organization"bson:"organization,omitempty"`
	//ContactPerson  string `json:"contact_person"bson:"contact_person,omitempty"`
	OrgLogo   string `json:"org_logo"bson:"org_logo"`
	Pid       string `json:"pid,omitempty"bson:"pid,omitempty"`
	Timestamp int    `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Seller Account
type BusinessInfo struct {
	Name    string `json:"name"bson:"name,omitempty"`
	Address string `json:"address"bson:"address,omitempty"`
	Phone   string `json:"phone"bson:"phone,omitempty"`
	Email   string `json:"email"bson:"email"`
}

type PaymentInfo struct {
	AccBeneficiary string `json:"beneficiary"bson:"beneficiary,omitempty"`
	AccNumber      string `json:"acc_number"bson:"acc_number,omitempty"`
	IFSC           string `json:"ifsc"bson:"ifsc,omitempty"`
	UPI            string `json:"upi"bson:"upi"`
	BankName       string `json:"bank"bson:"bank"`
	Branch         string `json:"branch"bson:"branch"`
}

type Selller struct {
	ID           bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Slug         string        `json:"slug"bson:"slug,omitempty"`
	Name         string        `json:"name"bson:"name,omitempty"`
	Phone        string        `json:"phone"bson:"phone,omitempty"`
	Email        string        `json:"email"bson:"email"`
	PanId        string        `json:"pan_no"bson:"pan_no"`
	GST          string        `json:"gstn_no"bson:"gstn_no"`
	BusinessName string        `json:"business_name"bson:"business_name"`
	Business     BusinessInfo  `json:"business_info"bson:"business_info"`
	Payment      PaymentInfo   `json:"payment_info"bson:"payment_info"`
	Timestamp    int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}
