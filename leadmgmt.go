package model

import "gopkg.in/mgo.v2/bson"

//Status represents mvpp
type Status struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Slug        string        `json:"slug"bson:"slug"`
	Name        string        `json:"name"bson:"name"`
	Description string        `json:"description"bson:"description"`
}

//Statues is an array of Status
type Statuses []Status

//Source represents mvpp
type Source struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Slug        string        `json:"slug"bson:"slug"`
	Name        string        `json:"name"bson:"name"`
	Description string        `json:"description"bson:"description"`
}

//Sources is an array of Source
type Sources []Source

//Contact Info represents mvpp
type Contact struct {
	ID      bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Source  Source        `json:"source,omitempty"bson:"source,omitempty"`
	Status  Status        `json:"status,omitempty"bson:"status,omitempty"`
	AddedBy Account       `json:"added_by,omitempty"bson:"added_by"`
	Slug    string        `json:"slug"bson:"slug"`
	Name    string        `json:"name"bson:"name"`
	Phone   string        `json:"phone"bson:"phone"`
	Email   string        `json:"email"bson:"email"`
	Address string        `json:"address"bson:"address"`
	Pincode string        `json:"pincode"bson:"pincode"`
	//OtherInfo			[]string `json:"other_info"bson:"other_info"`
	Timestamp int `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Contacts is an array of Contact
type Contacts []Contact

// Assign represents mvpp
type Assign struct {
	ID           bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Contact      bson.ObjectId `json:"contact,omitempty"bson:"contact,omitempty"`
	ContactInfo  Contact       `json:"contact_info,omitempty"bson:"contact_info,omitempty"`
	NextFollowup int           `json:"next_followup"bson:"next_followup"`
	UserTo       string        `json:"user_to,omitempty"bson:"user_to"`
	UserBy       string        `json:"user_by"bson:"user_by"`
	Timestamp    int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Assigns is an array of Assign
type Assigns []Assign

// Comment represents mvpp
type Comment struct {
	ID          bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Contact     bson.ObjectId `json:"contact,omitempty"bson:"contact,omitempty"`
	CommentData string        `json:"comment_data"bson:"comment_data"`
	StatusId    bson.ObjectId `json:"status_id"bson:"status_id"`
	Status      Status        `json:"status"bson:"status"`
	CreatedBy   Account   `json:"created_by,omitempty"bson:"created_by"`
	Attachment  string `json:"attachment"bson:"attachment"`
	Timestamp   int    `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Comments is an array of Comment
type Comments []Comment

//Convert represents mvpp
type Convert struct {
	ID        bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Contact   Contact       `json:"contact,omitempty"bson:"contact,omitempty"`
	AddedBy   Account          `json:"added_by"bson:"added_by"`
	Timestamp int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//Specifications is an array of Specification
type Converts []Convert

//Reference represents mvpp
type Reference struct {
	ID        bson.ObjectId `json:"_id,omitempty"bson:"_id,omitempty"`
	Contact   bson.ObjectId `json:"product,omitempty"bson:"product,omitempty"`
	Name      string        `json:"name"bson:"name"`
	Phone     string        `json:"phone"bson:"phone"`
	Email     string        `json:"email"bson:"email"`
	Address   string        `json:"address"bson:"address"`
	Timestamp int           `json:"timestamp,omitempty"bson:"timestamp,omitempty"`
}

//References is an array of Reference
type References []Reference
